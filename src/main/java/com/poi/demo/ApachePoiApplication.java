package com.poi.demo;

import com.poi.demo.slides.slide1;
import com.poi.demo.slides.slide2;
import com.poi.demo.slides.slide3;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ApachePoiApplication {

    public static void main(String[] args) throws IOException {
        slide1.createSlide1();
        slide2.createSlide2();
        slide3.createSlide3();
        SpringApplication.run(ApachePoiApplication.class, args);
    }

}
