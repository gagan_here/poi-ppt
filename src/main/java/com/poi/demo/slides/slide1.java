package com.poi.demo.slides;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.xslf.usermodel.*;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class slide1 {

    public static void createSlide1() throws IOException {

        //creating a new empty slide show
        XMLSlideShow ppt = new XMLSlideShow();

        //getting the slide master object
        XSLFSlideMaster slideMaster = ppt.getSlideMasters().get(0);

        //selecting a layot from list
        XSLFSlideLayout layout = slideMaster.getLayout(SlideLayout.TITLE_AND_CONTENT);

        //creating a slide with title and content layout
        XSLFSlide slide = ppt.createSlide(layout);

        //selection of title place holder
        XSLFTextShape title = slide.getPlaceholder(0);

        // remove predefined text
        title.clearText();

        //create a paragraph
        XSLFTextParagraph p = title.addNewTextParagraph();
        XSLFTextRun r = p.addNewTextRun();

        //setting title in it
        r.setText("COTIVITI");
        r.setFontColor(Color.decode("#6a0dad"));
        r.setBold(true);
        r.setUnderlined(true);

        //selection of body placeholder
        XSLFTextShape body = slide.getPlaceholder(1);

        //clearing the existing text in the slide
        body.clearText();

        //parsing json from file to string
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode node = objectMapper.readValue(JsonString.getJson(), JsonNode.class);
        String description = node.get("description").asText();

        //adding description
        XSLFTextRun rr = body.addNewTextParagraph().addNewTextRun();
        rr.setText(description);
        rr.setItalic(true);

        //creating FileOutputStream object
        File file = new File("C:\\Users\\Gagan\\Desktop\\poi\\slide.pptx");
        FileOutputStream out = new FileOutputStream(file);

        //saving changes to file
        ppt.write(out);
        out.close();
    }
}
