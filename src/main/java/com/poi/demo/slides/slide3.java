package com.poi.demo.slides;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.sl.usermodel.TextParagraph;
import org.apache.poi.xslf.usermodel.*;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class slide3 {
    public static void createSlide3() throws IOException {

        //opening an existing slide show
        File file = new File("C:\\Users\\Gagan\\Desktop\\poi\\slide.pptx");
        FileInputStream in = new FileInputStream(file);
        XMLSlideShow ppt = new XMLSlideShow(in);

        //getting the slide master object
        XSLFSlideMaster slideMaster = ppt.getSlideMasters().get(0);

        //selecting layot and adding a new slide
        XSLFSlideLayout layout = slideMaster.getLayout(SlideLayout.BLANK);
        XSLFSlide slide3 = ppt.createSlide(layout);

        //selection of title place holder
//        XSLFTextShape title = slide3.getPlaceholder(0);

        // remove predefined text
//        title.clearText();

        //create a paragraph
//        XSLFTextParagraph p = title.addNewTextParagraph();
//        XSLFTextRun r = p.addNewTextRun();

        //setting title in it
//        r.setText("Employee Details");
//        r.setFontColor(Color.MAGENTA);

//        XSLFTextBox text = slide3.createTextBox();
//        XSLFTextRun r = text.addNewTextParagraph().addNewTextRun();

        //selection of body placeholder
//        XSLFTextShape body = slide3.getPlaceholder(1);

        //clearing the existing text in the slide
//        body.clearText();

        //parsing json from file to string
        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode node = objectMapper.readValue(JsonString.getJson(), JsonNode.class);
        JsonNode detailsNode = node.get("details");

        XSLFTable table = slide3.createTable();
        table.setAnchor(new Rectangle(70, 100, 200, 100));

//        XSLFTableRow headerRow = table.addRow();
//        headerRow.setHeight(50);

//        for (int i = 0; i < detailsNode.size() + 1; i++) {
//
//            XSLFTableCell tableCell = headerRow.addCell();
//            XSLFTextParagraph pr = tableCell.addNewTextParagraph();
//            pr.setTextAlign(TextParagraph.TextAlign.CENTER);
//            XSLFTextRun tr = pr.addNewTextRun();
//
//            if (i == 0) {
//                tr.setText("First Name");
//            }
//            if (i == 1) {
//                tr.setText("Last Name");
//            }
//            if (i == 2) {
//                tr.setText("Profile");
//            }
//
////            XSLFTableCell tableCell = tableRow.addCell();
////            XSLFTextRun tr = tableCell.addNewTextParagraph().addNewTextRun();
////            tr.setText(detailsNode.get("firstName").asText());
//        }


        for (int i = 0; i < detailsNode.size(); i++) {

            XSLFTableRow row = table.addRow();
            row.setHeight(50);

            for (int j = 0; j < detailsNode.size() + 1; j++) {
                XSLFTableCell tableCell = row.addCell();
                XSLFTextParagraph pr = tableCell.addNewTextParagraph();
                XSLFTextRun tr = pr.addNewTextRun();
                JsonNode details = detailsNode.get(i);

                if (j == 0) {
                    tr.setText(details.get("firstName").asText());
                }
                if (j == 1) {
                    tr.setText(details.get("lastName").asText());
                }
                if (j == 2) {
                    tr.setText(details.get("profile").asText());
                    System.out.println("\n");
                }

//            XSLFTableCell tableCell = tableRow.addCell();
//            XSLFTextRun tr = tableCell.addNewTextParagraph().addNewTextRun();
//            tr.setText(detailsNode.get("firstName").asText());
            }
        }

//        String firstName = detailsNode.get("firstName").asText();
//        String lastName = detailsNode.get("lastName").asText();
//        String profile = detailsNode.get("profile").asText();

//        XSLFTable table = slide2.createTable();
//        table.setAnchor(new Rectangle2D.Double(10, 50, 0, 0));
//

        //adding description
//        body.addNewTextParagraph().addNewTextRun().setText("First Name: " + firstName);
//        body.addNewTextParagraph().addNewTextRun().setText("Last Name: " + lastName);
//        body.addNewTextParagraph().addNewTextRun().setText("Profile: " + profile);

        String image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABSgAAAEACAYAAACwIZjAAAAACXBIWXMAAAsTAAALEwEAmpwYAAAgAElEQVR42u3db2xd513A8Zu22ew1qy31D6hbE+PKk/3GxISWrV2kVVs9z5szj0iQRtrQSFVYozqeZpshBGEvysKLdsWtvW4pqmBTQYy27MVQJoSE0BAVYhpICMGLSQM0oYktJl5S8c78jvzc5tmdb9Lcc8/xyfr5Sl/1+D62I518/Zxzf/W9abUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANctR8NnwiMdj783PBbelj6eC2fDeacMAAAAAAAAQD84GO4P94bnwpvT4yfCD2WfNxqupOPFcNypAwAAAAAAAFCWO7Lj063tYeVd4dPhvvCGtFYMJafS8US47NQBAAAAAAAA6Cer4Z7wVPjZ8OHw5fD2tDaUPm8gXM+/8B+/9c/f2bhw8W/adn7jfM26devWrVu3bt26devWrVu3bv1q662XNrbIqmxi/2i1JsPj6fjx8M50fF9r+7cniwHlcHpsMFzLv/jdhw9vbfzwh+9p+2N/AdmadevWEy9t/Efhm//i/Pdy249bt15q3c+fdevWrVu3bt269et83RCNlQ4oG9j/G53ipdwns48fC+9Ox7eGnwkXWtvvV1lQvMR7qeN7bDmNwDXigsAqBQAAADxnIj1nuk5ov6R7b/JweCANJAvuDx8IR1qX33ey+I3KMQNKwMWWLrYAAACA50z0nAllORQ+EZ4Jn8wGjzPhR8IHs8+dTs7s8H38xQIutnSxBQAAADxnoudM2DX8xQIutnSxBQAAADxnoudMMKAEXGxJF1sAAAB4zkR6zmRACcDFli62AAAAgOdM9JwJBpSAiy1dbAEAAADPmUjPmQwoAbjY0sUWAAAA8JyJnjNhR46Gz4RHdlh7KDueC2fDeQNKwMWWLrYAAACA50z0nAn94GC4P9wbngtvztZmwhfS8Wi4ko4Xw3EDSsDFli62AADger4fPf9tsjI9Z6LnTLgG7siOT6dhZcEt4bHwuWwoOZWOJ8JlA0rAxZYutgAAwP0oWdn9qPNIz5nekKyGe9Lxw+FN2YCyWBtKxwPhev6Fv/DOd239YPPV+9t2fuN8rbb1lzZebXvDSxv/l5uvWbfe03o/+nVBYA0X21L761c2pj71zQsfzy0ey7Vuvef13bo/sG7dunXrr627Z2KV9qNf55FVN9q0/Rmt1mR4PB0fCu9Nx/mAcjgdD4Zr+Rd//a/+emtj8+Lfte385vlabet+2FjT/2npuV/nkTU0Wmp/dR5ZcaO7cn9g3bp169ZfW3c9YpX2o1/nkVU32rT9+Y3OvvBk9vGJcCn5Smv7H8dZaG2/X2XBRFrLad6vxvpho5crUKMapZfUALvJn33/bWRlutbT/SjpfvQniOIl3ada2/9ITuHhjvVn039HWpffd7J4P8oxA0rayDRKjZJuCAH7KF3rSY3S/SjKUryc+4nwTPjkDoPH49nxdHJmh+9jQEkbmUapUdINIWAfpWs9qVG6H8WuYUBJG5lGqVHSDSFgH6VrPalRuh+FAaWNjC62pEbphhBwP0r7qEapUdL9qAGlG0LayDRKjZJuCGFASbrWU6MapftRGFCSLrbUqEbphhBwP0r7qEapUdL9qAGlG0LayDRKjZJuCGFASbrWU6MapftRGFCSLrbUqEbphhBwP0r7qEapUWoUzeJo+Ex45CqPzYWz4bwBJW1kGqVGSTeEgH2UrvWkRul+FP3gYLg/3BueC2/u8thouJK+ZjEcN6CkjUyj1CjphhCwj9K1ntQo3Y+iLHdkx6fTYHKnx4qh5FR6bCJcNqCkjUyj1CjphhCwj9K1ntQo3Y+in6yGe7o8Vvx3KD02EK7nn/SVF1/e2ti89JdtO79xvlbbuh821rSR9dyv88gaGi21vzqPrLjRXbk/sG69xnU/66zSfvTrPLKORsvsr84jq260afcPaLUmw+NXeKwYUA6n48FwLf/EB6ffv/WDCxdn23Z+83yttnU/bKxpQNlzv84ja2i01P7qPLLiRnfl/sC69RrX/ayzSvvRr/PIOhots786j6y60abdP7zR2ReevMpjC63t96Zsv8R7yUu8afijUWqU9JIawD5K13pSo3Q/irIUL98+1dr+B3EKD3d5bKR1+X0ni/ejHDOgpI1Mo9Qo6YYQsI/StZ7UKN2PoiyHwifCM+GTafC402MF08mZHb6PASVtZBqlRkk3hIB9lK71pEbpfhS7hgElbWQapUZJN4SAfZSu9aRG6X4UBpQ2MrrYkhqlG0LA/SjtoxqlRkn3owaUbghpI9MoNUq6IYQBJelaT41qlO5HYUBJuthSoxrlT2ijL57/OlmZ9lG61pMapUZhQGkjo4stqVFqVKPUKKlRapQ0oIQBJeliS42SGqVGNUqNkhqlRlEpR8NnwiPZY3PhbDh/lccMKGkj0yg1SmqUGtUoNUpqlAaU6JmD4f5wb3guvDkcDVfS+mI43uUxA0rayDRKjZIapUY1So2SGqUBJUpxR3Z8Og0riwHkVHpsIlzu8thrTE7+7NbGxsWptp1/SL5W27ofNta0kfXcr/PIGhottb86j6y40bLXf+eRVdqP+1PnkXU12uvzJ+eRdTRa5vm988iqG23E/Ao/xmq4J/13KD02EK53eew1/vYbf791/sKlb7Xt/Mb5Wm3rfthY04Cy536dR9bQaKn91XlkxY2Wvf47j6zSftyfOo+sq9Fenz85j6yj0TLP751HVt1oI+ZX+BEmw+PZoHI4HQ+Ga10e8xJvGlBqlBolNUqNapQaJTVKL/FGafaFJ7OPF9J7U7Zfzr3U5TEDStrINEqNkhqlRjVKjZIapQElSlG8pPtUa/sfySk8HI5k7zFZvPfkWJfHDChpI9MoNUpqlBrVKDVKapQGlCjFofCJ8Ez4ZDZ4nE7OZJ+702MGlLSRaZQaJTVKjWqUGiU1SgNK7DoGlLSRaZQaJTVKjWqUGiU1SgNKGFDayOhiS2qUGiU1So1qlBolDSgNKA0oaSPTKDVKapQaJTVKjWqUBpQwoCRdbKlRjVKjpEapUY1So6QBpQGlASVtZBqlRkmNUqOkRqlRjdKAEjtyIHyk47H3hR8M57PH5sLZjscMKGkj0yg1SmqUGtUoNUpqlAaUKMX+8Pns4/HwPen4Y+Ht4Wi4kh5bTJ9jQEkbmUapUVKj1KhGqVFSozSgRF94Kju+M3w8HX8yfFMaSk6lxybCZQNK2sg0So2SGqVGNUqNkhqlASWqGFAWnA6/Gh5MH6+GQ+l4IFzPP/mPv/zC1vkLF19s2/nN87Xa1v2wsaaNrOd+nUfW0Gip/dV5ZMWNlr3+O4+s0n7cnzqPrKvRXp8/OY+so9Eyz++dR1bdaCPmV+g6oLwp/ER4NPxa+NY0oBxO64PhWv7FRz48v7Wxeelo285vnq/Vtu6HjTUNKHvu13lkDY2W2l+dR1bcaNnrv/PIKu3H/anzyLoa7fX5k/PIOhot8/zeeWTVjTZifoWuA8qPhu9MxzPhp8OF7Lcpi5d4L3mJNw1/NEqNkhqlRjVKjZIapZd4o1+czY6PhR9Ixz8TPhqOZO87Wbwf5ZgBJW1kGqVGSY1SoxqlRkmN0oAS/eCe9BLuyfTxnvRblDNpUHljenw6ObPD9zCgpI1Mo9QoqVFqVKPUKKlRGlBi1zCgpI1Mo9QoqVFqVKPUKKlRGlDCgNJGRhdbUqPUKKlRalSj1ChpQGlAaUBJG5lGqVFSo9QoqVFqVKM0oIQBJeliS41qlBolNUqNapQaJQ0oDSgNKGkj0yg1SmqUGiU1So1qlAaUMKAkXWypUY1So6RGqVGNUqPUKJrFgfCRHR5/b3gsvC19PBfOhvMGlLSRaZQaJTVKjWqUGiU1SgNK9Iv94fMdj50IP5R9PBqupOPFcNyAkjYyjVKjpEapUY1So6RGaUCJfvFUdnxX+HS4L7whG0pOpeOJcDn/4rF3vGPrfzY3x9t2fvN8rbZ1P2ysaSPruV/nkTU0Wmp/dR5ZcaNlr//OI6u0H/enziPrarTX50/OI+totMzze+eRVTfaiPkVug4oT4WfDR8OXw5vD1fDobQ+EK7nX/wP3/ynrfObl/6tbec3z9dqW/fDxpoGlD336zyyhkZL7a/OIytutOz133lklfbj/tR5ZF2N9vr8yXlkHY2WeX7vPLLqRhsxv0LXAeXj4Z3p+L7025PFgHI4PTYYrnmJNw1/NEqNkhqlRjVKjZIapZd4o4oB5WPh3en41vAz4UJ4MHuJ95IBJW1kGqVGSY1SoxqlRkmN0oAS/eJsdnwgDSQL7g8fCEey950sfqNyzICSNjKNUqOkRqlRjVKjpEZpQIl+cE96Cfdk9thM+JHwweyx6eTMDt/DgJI2Mo1So6RGqVGNUqOkRmlAiV3DgJI2Mo1So6RGqVGNUqOkRmlACQNKGxldbEmNUqOkRqlRjVKjpAGlAaUBJW1kGqVGSY1So6RGqVGN0oASBpSkiy01qlFqlNQoNapRapQ0oDSgNKCkjUyj1CipUWqU1Cg1qlEaUGJHDoSPdFl7KDueC2fDeQNK2sg0So2SGqVGNUqNkhqlASX6xf7w+R0enwlfSMej4Uo6XgzHDShpI9MoNUpqlBrVKDVKapQGlOgXT3V8fEt4LHwuG0pOpeOJcNmAkjYyjVKjpEapUY1So6RGaUCJqgaUD4c3ZQPK1XAoHQ+E6/knf+HsH25tbF76UtvOb56v1bbuh401bWQ99+s8soZGS+2vziMrbrTs9d95ZJX24/7UeWRdjfb6/Ml5ZB2Nlnl+7zyy6kYbMb9C1wHlofDedJwPKIfT8WC4ln/xLx97aOv8hYsfbdv5zfO12tb9sLGmAWXP/TqPrKHRUvur88iKGy17/XceWaX9uD91HllXo70+f3IeWUejZZ7fO4+sutFGzK/QdUB5IlxKvtLa/sdxFsKD2Uu8l7zEm4Y/GqVGSY1SoxqlRkmN0ku80S/Odnn82fTfkex9J4v3oxwzoKSNTKPUKKlRalSj1CipURpQoh/ck17CPbnD2vHseDo5s8PnGVDSRqZRapTUKDWqUWqU1CgNKLFrGFDSRqZRapTUKDWqUWqU1CgNKGFAaSOjiy2pUWqU1Cg1qlFqlDSgNKA0oKSNTKPUKKlRapTUKDWqURpQwoCSdLGlRjVKjZIapUY1So2SBpQGlAaUtJFplBolNUqNkhqlRjVKA0oYUJIuttSoRqlRUqPUqEapUWoUzeJA+EjHY0fDZ8Ij2WNz4Ww4b0BJG5lGqVFSo9SoRqlRUqM0oES/2B8+n318MD22NzwX3hyOhitpfTEcN6CkjUyj1CipUWpUo9QoqVEaUKJfPJUd35Edn07DymIoOZUemwiX8y9++9vv2jp//tX9bTu/eb5W27ofNta0kfXcr/PIGhottb86j6y40bLXf+eRVdqP+1PnkXU12uvzJ+eRdTRa5vm988iqG23E/ApdB5Q5q+Ge9N+h9NhAuJ5/0r/8679vbVy4+J9tO79Jvlbbuh821jSg7Llf55E1NFpqf3UeWXGjZa//ziOrtB/3p84j62q01+dPziPraLTM83vnkVU32oj5Fa46oJwMj2eDyuF0PBiueYk3DX80So2SGqVGNUqNkhqll3ijqgHlvvBk9vFCem/K9ku8lwwoaSPTKDVKapQa1Sg1SmqUBpToF2ez4+Il3ada2/9ITuHhcCR738ni/SjHDChpI9MoNUpqlBrVKDVKapQGlOgH96SXcE+mjw+FT4RnwiezYeR0cmaH72FASRuZRqlRUqPUqEapUVKjNKDErmFASRuZRqlRUqPUqEapUVKjNKCEAaWNjC62pEapUVKj1KhGqVHSgNKA0oCSNjKNUqOkRqlRUqPUqEZpQAkDStLFlhrVKDVKapQa1Sg1ShpQGlAaUNJGplFqlNQoNUpqlBrVKA0osSMHwkc6HpsLZ8P5qzxmQEkbmUapUVKj1KhGqVFSozSgRCn2h89nH4+GK+l4MRzv8pgBJW1kGqVGSY1SoxqlRkmN0oASfeGp7LgYQE6l44lwuctjBpS0kWmUGiU1So1qlBolNUoDSvR9QLkaDqXjgXC9y2Ov8bk/eHrr/IVLX2jb+c3ztdrW/bCxpo2s536dR9bQaKn91XlkxY2Wvf47j6zSftyfOo+sq9Fenz85j6yj0TLP751HVt1oI+ZXuOKAcjgdD4ZrXR57jY//6omt/9289GttO795vlbbuh821jSg7Llf55E1NFpqf3UeWXGjZa//ziOrtB/3p84j62q01+dPziPraLTM83vnkVU32oj5FboOKBfCg9nLuZe6POYl3jSg1Cg1SmqUGtUoNUpqlF7ijb5wNjseyd5jsnjvybEujxlQ0kamUWqU1Cg1qlFqlNQoDShRmnvSS7gns8emkzNXecyAkjYyjVKjpEapUY1So6RGaUCJXceAkjYyjVKjpEapUY1So6RGaUAJA0obGV1sSY1So6RGqVGNUqOkAaUBpQElbWQapUZJjVKjpEapUY3SgBIGlKSLLTWqUWqU1Cg1qlFqlDSgNKA0oKSNTKPUKKlRapTUKDWqURpQwoCSdLGlRjVKjZIapUY1So1So2g+7ws/GM5nj82Fsx2PGVDSRqZRapTUKDWqUWqU1CgNKNFXxsP3pOOPhbeHo+FKemwxfY4BJW1kGqVGSY1SoxqlRkmN0oASfefO8PF0/MnwTWkoOZUemwiX8y+49dZbtzY3N29r2/kN87Xa1v2wsaaNrOd+nUfW0Gip/dV5ZMWNlr3+O4+s0n7cnzqPrKvRXp8/OY+so9Eyz++dR1bdaCPmV7gip8OvhgfTx6vhUDoeCNfzT/72d/5r6/zmpe+37fxm+Vpt637YWNOAsud+nUfW0Gip/dV5ZMWNlr3+O4+s0n7cnzqPrKvRXp8/OY+so9Eyz++dR1bdaCPmV+jKTeEnwqPh18K3pgHlcFofDNe8xJuGPxqlRkmNUqMapUZJjdJLvFEFHw3fmY5nwk+HC9lvUxYv8V4yoKSNTKPUKKlRalSj1CipURpQogqOhR9Ixz8TPhqOZO87Wbwf5ZgBJW1kGqVGSY1SoxqlRkmN0oASVbAn/RblTBpU3pgen07O7PA1BpS0kWmUGiU1So1qlBolNUoDSuwaBpS0kWmUGiU1So1qlBolNUoDShhQ2sjoYktqlBolNUqNapQaJQ0oDSgNKGkj0yg1SmqUGiU1So1qlAaUMKAkXWypUY1So6RGqVGNUqOkAaUBpQElbWQapUZJjVKjpEapUY3SgBLXxHvDY+Ft6eO5cDacN6CkjUyj1CipUWpUo9QoqVEaUKJKToQfyj4eDVfS8WI4bkBJG5lGqVFSo9SoRqlRUqM0oEQV3BU+He4Lb8iGklPpeCJcNqCkjUyj1CipUWpUo9QoqVEaUKIKToWfDR8OXw5vD1fDobQ+EK7nX/B7Z35/6/zmxc+17fyG+Vpt637YWNNG1nO/ziNraLTU/uo8suJGy17/nUdWaT/uT51H1tVor8+fnEfW0WiZ5/fOI6tutBHzK3Tl8fDOdHxf+u3JYkA5nB4bDNfyL3j05GNbGxcufbJt5zfM12pb98PGmgaUPffrPLKGRkvtr84jK2607PXfeWSV9uP+1HlkXY32+vzJeWQdjZZ5fu88supGGzG/QlceC+9Ox7eGnwkXwoPZS7yXvMSbhj8apUZJjVKjGqVGSY3SS7xRBQfSQLLg/vCBcCR738niNyrHDChpI9MoNUpqlBrVKDVKapQGlKiKmfAj4YPZY9PJmR0+34CSNjKNUqOkRqlRjVKjpEZpQIldw4CSNjKNUqOkRqlRjVKjpEZpQAkDShsZXWxJjVKjpEapUY1So6QBpQGlASVtZBqlRkmNUqOkRqlRjdKAEgaUpIstNapRapTUKDWqUWqUNKA0oDSgpI1Mo9QoqVFqlNQoNapRGlDCgJJ0saVGNUqNkhqlRjVKjVKjuH54KDueC2fDeQNK2sg0So2SGqVGNUqNkhqlASWqZiZ8IR2PhivpeDEcN6CkjUyj1CipUWpUo9QoqVEaUKIqbgmPhc9lQ8mpdDwRLuefPDj4lq3vfnfrLW07v1m+Vtu6HzbWtJH13K/zyBoaLbW/Oo+suNGy13/nkVXaj/tT55F1Ndrr8yfnkXU0Wub5vfPIqhttxPwKV+Th8KZsQLkaDqXjgXA9/+T//t73tzYuXHy1bec3y9dqW/fDxpoGlD336zyyhkZL7a/OIytutOz133lklfbj/tR5ZF2N9vr8yXlkHY2WeX7vPLLqRhsxv0JXDoX3puN8QDnc/oXJcM1LvGn4o1FqlNQoNapRapTUKL3EG1VwIlxKvtLa/sdxFsKD2Uu8lwwoaSPTKDVKapQa1Sg1SmqUBpSommfTf0ey950s3o9yzICSNjKNUqOkRqlRjVKjpEZpQImqOZ4dTydndvg8A0rayDRKjZIapUY1So2SGqUBJXYNA0rayDRKjZIapUY1So2SGqUBJQwobWR0sSU1So2SGqVGNUqNkgaUBpQGlLSRaZQaJTVKjZIapUY1SgNKGFCSLrbUqEapUVKj1KhGqVHSgNKA0oCSNjKNUqOkRqlRUqPUqEZpQInXzdHwmfBI9thcOBvOG1DSRqZRapTUKDWqUWqU1CgNKFEVB8P94d7wXHhzOBqupPXFcNyAkjYyjVKjpEapUY1So6RGaUCJKrgjOz6dhpXFUHIqPTYRLhtQ0kamUWqU1Cg1qlFqlNQoDShRNavhnvTfofTYQLief9Jv/87vbm1cuPR4285vkq/Vtu6HjTVtZD336zyyhkZL7a/OIytutOz133lklfbj/tR5ZF2N9vr8yXlkHY2WeX7vPLLqRhsxv8JVmQyPZ4PK4XQ8GK7ln/ip5ZWt85sXf6tt5zfK12pb98PGmgaUPffrPLKGRkvtr84jK2607PXfeWSV9uP+1HlkXY32+vzJeWQdjZZ5fu88supGGzG/whXZF57MPl5I703Zfon3kpd40/BHo9QoqVFqVKPUKKlReok3qqB4Sfep1vY/klN4OBzJ3neyeD/KMQNK2sg0So2SGqVGNUqNkhqlASWq4FD4RHgmfDIbRk4nZ3b4GgNK2sg0So2SGqVGNUqNkhqlASV2DQNK2sg0So2SGqVGNUqNkhqlASUMKG1kdLElNUqNkhqlRjVKjZIGlAaUBpS0kWmUGiU1So2SGqVGNUoDShhQki621KhGqVFSo9SoRqlR0oDSgNKAkjYyjVKjpEapUVKj1KhGaUAJA0rSxZYa1Sg1SmqUGtUoNUqN4vpkLpwN5w0oaSPTKDVKapQa1Sg1SmqUBpSok9FwJR0vhuMGlLSRaZQaJTVKjWqUGiU1SgNK1EUxlJxKxxPhsgElbWQapUZJjVKjGqVGSY3SgBJ1sRoOpeOBcD1f3Ni8tJWbBpavad26devWrVu3bt26devWrVu3bt269e7rjfwFwAYOKIfT8WC41vjfoMS14u8QGgU0Co0CGgU0Co3C32FjWQgPpuPiJd5LTqAfAkCjgEahUUCj0CigUfg7rIuR1uX3nSzej3LMCfRDAGgU0Cg0CmgUGgU0Cn+HdTKdnHEC/RAAGgU0Co0CGoVGAY3C36ETCH+H0CigUUCj0CigUUCj/g4BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAbi6PhM+GR7LG5cDaczx47ED7S5Xs8dJU/48Ph+9N/r/TnAk1q9IHwsfCYvwI0tNHX+7VAmUZ/KlwLPx/+3DU2eqXmgSY06n4UTW/U/Sia3qj7UQB94WC4P9wbngtvDkfDlbS+GI6n4+Lznt/hexT/OvsLV/gzio3qvnT87vTxTn8u0KRGD6QLdcG6vwY0sNHX+7VA2UYfDd9ylT+jW6Pdmgea0Kj7UTS9UfejuB6u9e5HAfSFO7Lj02mzKjavqfTYRLicfc5THV9/S2v7/+Y9d4U/4486Pv5ylz8XaFKjOX77B01t9PV8LVCm0T3hN8LfTL31so8+5a8ADW3U/Siul33U/Sia3Kj7UQB9ZzVtTsV/h9JjA60f/b91nU8yHg5vuspm9KcdH/95lz8XaFqjxf9FfDr8DaceDW309XwtULbRt6Unxl+7wvX6Std6A0o0vVH3o2hyo+5H0fRG3Y8C6CuT4fFsQxtOx4Ot7fei2GkjOxTem47zzaj4VfKfT/8t+JOOP+vFLn8u0MRGbwy/Go75K0DDGu32tUA/G835ldTdte6jBpRoeqPuR9H0Rt2PoqmNuh8F0Ff2hSezjxda2+9hUVD8KvhSl43sRForfKW1/Qa8BcWb8n66dfnNeb/Y8ed9qcufCzSt0TbFG0u/y18DGtZot68F+tlozj2ty+89dS37qAElmtyo+1FcD/uo+1E0tVH3owD6RvHr26da22+mW3g4HGldfn+K4n0r8v9Td7bL93n2Cn/Gg+H70vF0a/tfENvpzwWa1OiNrcsvbyj+T+Kgvwo0rNHX+7VAmUaLl2zdkI5/MXxzD42e9VeAhjbqfhRNb9T9KK6Xa737UQClKX4l+4nwTPhktmlNJ2eyzy3+b0rxa+KTO3yfq70sptjIvpi+55X+XKApjY6nP++XWtvvyQI0rdFr+VpotNdG7w4/n/bCn+6h0Ss1D+x2o+5H0fRG3Y/ierjWu3U3AHwAAADISURBVB8FcN3xQPjrLS9NgEYBjUKjgEYBjUKjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAz/w96Bx+hAA7KOgAAAABJRU5ErkJggg==";
        byte[] data = Base64.decodeBase64(image.substring(image.indexOf(",") + 1));
        XSLFPictureData idx = ppt.addPicture(data, PictureData.PictureType.PNG);

        XSLFPictureShape pic = slide3.createPicture(idx);
        pic.setAnchor(new Rectangle(470, 300, 250, 197));

        //Saving changes
        FileOutputStream out = new FileOutputStream(file);
        ppt.write(out);

        out.close();
    }

}

