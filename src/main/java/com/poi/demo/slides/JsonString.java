package com.poi.demo.slides;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JsonString {

    public static String getJson() throws IOException {
        return FileUtils.readFileToString(
                new File("C:\\Users\\Gagan\\Desktop\\mix\\Apache POI\\src\\main\\resources\\sample.json"),
                StandardCharsets.UTF_8);
    }

}
